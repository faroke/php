function somecarre_iterative($n){
    $stock = 0;
    for($i = 1; $i <= $n; $i++)
    {
        $stock += $i ** 2;
    }
    return $stock;
}

function somecube_iterative($n){
    $stock = 0;
    for($i = 1; $i <= $n; $i++)
    {
        $stock += $i**3;
    }
    return $stock;
}

function somquatre_iterative($n){
    $stock = 0;
    for($i = 1; $i <= $n; $i++)
    {
        $stock += $i**4;
    }
    return $stock;
}
